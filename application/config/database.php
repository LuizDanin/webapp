<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> 'mysql:host=127.0.0.1;port=3306 ;dbname=webapp_db; charset=utf8;',
	'hostname' => '127.0.0.1',
	'username' => 'webapp',
	'password' => '#webapp#',
	'database' => 'webapp_db',
	'dbdriver' => 'pdo',
	'dbprefix' => '',
	'pconnect' => FALSE ,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => '	utf8mb4_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);