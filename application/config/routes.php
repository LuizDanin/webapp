<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Inicio/';
$route['login'] = 'Inicio/do_login';
$route['acesso'] = 'Usuario/pagina_acesso';
$route['historico'] = 'Usuario/pagina_historico';
$route['register'] = 'Usuario/do_register';

$route['sair'] = 'Usuario/do_logout';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;