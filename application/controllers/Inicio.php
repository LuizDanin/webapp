<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }
    
    public function index()
    {
        $this->load->view('inicio/include/head.php');
        $this->load->view('inicio/pagina/home');
        $this->load->view('inicio/include/footer.php');
    }
    
    private function verifica_login($email,$senha)
    {
        $this->load->model('Usuario_model');
        
        return $this->Usuario_model->verifica_login($email,$senha);
    }
    
    public function do_login()
    {
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[4]');
        
            if ( $this->form_validation->run() )
            {
                $email = $this->input->post("email", TRUE);
                $email = trim($email);
                $email = strip_tags($email);
                $email = filter_var($email, FILTER_SANITIZE_STRING);
                $email = str_replace(['""','WHERE','1=1', '%', '`','´', '*',';','/'], '', $email);

                $senha = $this->input->post('senha', TRUE);
                $senha = trim($senha);
                $senha = strip_tags($senha);
                $senha = filter_var($senha, FILTER_SANITIZE_STRING);
                $senha = str_replace(['""','WHERE','1=1', '%', '`','´', '*',';','/'], '', $senha);
                
                $user = $this->verifica_login($email, $senha);
                if( isset($user[0]) && $user[0]['id_categoria_usuario']=='1')
                {
                //echo '<pre>';var_dump($user);die;
                    $data = [
                                'id_usuario' => $user[0]['id'],
                                'id_categoria_usuario'=>$user[0]['id_categoria_usuario'],
                                'email' => $user[0]['email'],
                                'data'=>date("Y-m-d H:i:s"),
                                'logado'=>TRUE
                                ];

                    $this->session->set_userdata($data);
                    redirect("acesso");
                }
                
            }
        
        }
        
        redirect('acesso');
    }

}
