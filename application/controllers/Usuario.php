<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->seguranca();
    }
    
    private function seguranca()
    {
        if( !$this->session->has_userdata('logado') || !$this->session->userdata('logado') )
        {
                $this->session->sess_destroy();
                redirect();
        }
    }
    
    public function do_logout()
    {
        $this->session->sess_destroy();
        redirect();
    }
    
    public function pagina_acesso()
    {
       //$id_situacao = 1;
       //$is_acesso = $this->historico_acesso( $user[0]['id'], $id_situacao); 
        
        $id_usuario = $this->session->userdata('id_usuario');
        
        $this->load->model('Usuario_model');
        $select_historico = $this->Usuario_model->select_historico($id_usuario);
        //echo '<pre>';var_dump($select_historico_hoje);        die();
        $data = ['historico'=>$select_historico];
        
        $this->load->view('usuario/include/head.php');
        $this->load->view('usuario/pagina/acesso', $data);
        $this->load->view('usuario/include/footer.php');
    }
    
    private function get_value_week($data_week)
    {
        $data_week = explode("-", $data_week);
        
        $ano = (int)$data_week[0];
        $mes = (int)$data_week[1];
        $dia = (int)$data_week[2];
        
        $jd = gregoriantojd( $mes, $dia, $ano);
        $dia_semana = jddayofweek($jd,1);
        
        switch ($dia_semana)
        {
        case 'Sunday':
            return "Domingo";
        case 'Monday':
            return "Segunda-feira";
        case 'Tuesday':
            return "Terça-feira";
        case 'Wednesday':
            return "Quarta-feira";
        case 'Thursday':
            return "Quinta-feira";
        case 'Friday':
            return "Sexta-feira";
        case 'Saturday':
            return "Sábado";
        }
    }
    
    private function build_historico_agenda($dados)
    {
        $agenda = '';
        $dia = strtotime($dados[0]['data']);
        $diad = date("Y-m-d", $dia);
        
        for ($i = 0; $i < count($dados); )
        {
            $dia_data = strtotime($dados[$i]['data']);
            $dia_data = date("Y-m-d", $dia_data);
            
            $dia = strtotime($dados[$i]['data']);
            $dia = date("d", $dia);
            
            $dia_semana = $this->get_value_week($dia_data);
            
            if( isset($dados[$i]) )
            {
                
            $agenda .= '<tr>
                <td class="agenda-date" class="active" rowspan="3">
                    <div class="dayofmonth">'.$dia.'</div>
                    <div class="dayofweek">'.$dia_semana.'</div>
                    <div class="shortdate text-muted">'.date("m",strtotime($dados[$i]['data'])).', '.date("Y",strtotime($dados[$i]['data'])).'</div>
                </td>
                <td class="agenda-time">
                    '.date("H:i", strtotime($dados[$i]['hora_final']) ).' PM 
                </td>
                <td class="agenda-events">
                    <div class="agenda-event">
                        Saída
                    </div>
                </td>
               </tr>';
            }
            if( isset($dados[$i+1]) )
            {
                $agenda .=    '<tr>
                        <td class="agenda-time">
                            '.date("H:i", strtotime($dados[$i+1]['hora_inicial'])).' - '.date("H:i", strtotime($dados[$i+1]['hora_final'])).' PM 
                        </td>
                        <td class="agenda-events">
                            <div class="agenda-event">
                                Almoço
                            </div>
                        </td>
                    </tr>';
            }
            if( isset($dados[$i+2]) )
            {
                
            $agenda .=    '<tr>
                    <td class="agenda-time">
                        '.date("H:i", strtotime($dados[$i+2]['hora_inicial'])).' AM
                    </td>
                    <td class="agenda-events">
                        <div class="agenda-event">
                            Entrada
                        </div>
                    </td>
                </tr>';
            }
                        
            $i = $i+3;
        }
        
        return $agenda;
    }
    
    public function pagina_historico()
    {
        $id_usuario = $this->session->userdata('id_usuario');
        
        $this->load->model('Usuario_model');
        $select_historico_agenda = $this->Usuario_model->select_historico_agenda($id_usuario);
        //echo '<pre>';var_dump($select_historico_agenda);die;
        $agenda = '';
        if (isset($select_historico_agenda[0]))
        {
            $agenda = $this->build_historico_agenda($select_historico_agenda);
        }
        $data = ['agenda'=>$agenda];
        
        $this->load->view('usuario/include/head.php');
        $this->load->view('usuario/pagina/historico', $data);
        $this->load->view('usuario/include/footer.php');
    }
    
    private function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    private function build_data($situacao, $hora_inicial=NULL, $hora_final=NULL)
    {
        
        $hora_inicial = (isset($hora_inicial) ) ? date("H:i:s",strtotime($hora_inicial) ) : NULL;
        $hora_final = (isset($hora_final) ) ? date("H:i:s",strtotime($hora_final)): NULL;
        
        $ip = $this->get_client_ip();
        
        return [
                'id_usuario'=>(int)$this->session->userdata('id_usuario'),
                'id_situacao_acesso'=>(int)$situacao,
                'hora_inicial'=>$hora_inicial,
                'hora_final'=>$hora_final,
                'ip' => ($ip!='UNKNOWN') ? $ip : getenv("REMOTE_ADDR"), 
                'is_robot'=> $this->agent->is_robot(),
                'is_mobile'=> $this->agent->is_mobile(),
                'agent_string'=> $this->agent->agent_string(),
                'plataforma'=>$this->agent->platform(),
                'navegador' => $this->agent->browser() . ' - ' . $this->agent->version(), 
                'sistema' => $this->agent->platform(),
                'data' => date("Y-m-d H:i:s")
               ];
    }

    public function do_register()
    {
        
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            $situacao = $this->input->post('situacao', TRUE);
            
            $this->load->library("form_validation");
        
            $this->load->model('Usuario_model');
            
            $id_usuario = $this->session->userdata('id_usuario');
            
            if($situacao == '1')
            {
                $chave = 0;
                $this->check_validate_register($id_usuario, $situacao, $chave);
                
                $h_entrada = $this->input->post('h_entrada', TRUE) ?? '';
                $this->form_validation->set_rules('h_entrada', 'h_entrada', 'trim|required');
                
                if ( $this->form_validation->run() )
                {

                    $data = $this->build_data($situacao, $h_entrada);
                    $this->Usuario_model->register_historico_acesso($data);
                    redirect('acesso');
                }

            }
            elseif ($situacao == '2')
            {
                $this->check_historico_usuario_by_situacao($id_usuario, 1);
                
                $chave = 1;
                #metodo pra que não seja insrido mais de uma vez o mes
                $this->check_validate_register($id_usuario, $situacao, $chave);
                
                $h_almoco_saida = $this->input->post('h_almoco_saida', TRUE) ?? '';
                $h_almoco_retorno = $this->input->post('h_almoco_retorno', TRUE) ?? '';
                
                $this->form_validation->set_rules('h_almoco_saida', 'h_almoco_saida', 'trim|required');
                $this->form_validation->set_rules('h_almoco_retorno', 'h_almoco_retorno', 'trim|required');
                
                if ( $this->form_validation->run() )
                {
                    $data = $this->build_data($situacao, $h_almoco_saida, $h_almoco_retorno); 
                    $this->Usuario_model->register_historico_acesso($data);
                    redirect('acesso');
                }
            }
            elseif ($situacao == '3') 
            {
                $this->check_historico_usuario_by_situacao($id_usuario, 2);
                
                $chave = 2;
                $this->check_validate_register($id_usuario, $situacao, $chave);
                
                $h_saida = $this->input->post('h_saida', TRUE) ?? '';
                $this->form_validation->set_rules('h_saida', 'h_saida', 'trim|required');
                
                if ( $this->form_validation->run() )
                {
                    $data = $this->build_data($situacao, NULL, $h_saida);
                    $this->Usuario_model->register_historico_acesso($data);
                    redirect('acesso');
                }
            }
            
            redirect('acesso');
        }
        
        redirect( $_SERVER['HTTP_REFERER'] );
        
    }
    
    private function check_historico_usuario_by_situacao($id_usuario, $id_situacao)
    {
        $select_historico = $this->Usuario_model->selec_historico_usuario_by_situacao($id_usuario, $id_situacao);
        
        if( !isset($select_historico[0]) )
        {
            redirect('acesso');
        }
    }

    private function check_validate_register($id_usuario, $id_situacao_acesso, $chave)
    {
        $select_historico = $this->Usuario_model->select_historico($id_usuario);
        
        if (isset($select_historico[$chave]) && ($select_historico[$chave]['id_situacao_acesso']==$id_situacao_acesso) )
        {
            redirect( $_SERVER['HTTP_REFERER'] );
        }
    }
    
}