<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function verifica_login($email, $senha)
    {
        $this->db->select();
        
        $this->db->from('usuario');
        
        $this->db->where('email',$email);
        $this->db->where('senha',$senha);
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function register_historico_acesso($historico_acesso)
    {
        $sql = "INSERT INTO historico_acesso (id_usuario, id_situacao_acesso, hora_inicial, hora_final, ip, is_robot, is_mobile, agent_string, plataforma, navegador, sistema, data) "
                . "VALUES( :id_usuario, :id_situacao_acesso, :hora_inicial, :hora_final, :ip, :is_robot, :is_mobile, :agent_string, :plataforma, :navegador, :sistema,:data)";
        
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_usuario', $historico_acesso['id_usuario'], PDO::PARAM_INT); 
        $sth->bindValue(':id_situacao_acesso', $historico_acesso['id_situacao_acesso'], PDO::PARAM_INT);
        $sth->bindValue(':hora_inicial', $historico_acesso['hora_inicial'], PDO::PARAM_STR);
        $sth->bindValue(':hora_final', $historico_acesso['hora_final'], PDO::PARAM_STR);
        $sth->bindValue(':ip', $historico_acesso['ip'], PDO::PARAM_STR);
        $sth->bindValue(':is_robot', $historico_acesso['is_robot'], PDO::PARAM_BOOL);
        $sth->bindValue(':is_mobile', $historico_acesso['is_mobile'], PDO::PARAM_BOOL);
        $sth->bindValue(':agent_string', $historico_acesso['agent_string'], PDO::PARAM_STR);
        $sth->bindValue(':plataforma', $historico_acesso['plataforma'], PDO::PARAM_STR);
        $sth->bindValue(':navegador', $historico_acesso['navegador'], PDO::PARAM_STR);
        $sth->bindValue(':sistema', $historico_acesso['sistema'], PDO::PARAM_STR);
        $sth->bindValue(':data', $historico_acesso['data'], PDO::PARAM_STR);
        
        if( $sth->execute() )
        {
            return TRUE;
        }
        return FALSE;
    }
    
    public function select_historico($id_usuario)
    {
        $this->db->select();
        $this->db->from('historico_acesso');
        
        $this->db->where('id_usuario', $id_usuario );
        $this->db->where('data BETWEEN "'. date('Y-m-d'). '" AND "'. date('Y-m-d 23:59:59').'"' );
        
        $this->db->order_by('id_situacao_acesso', 'ASC');
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    public function select_historico_agenda($id_usuario)
    {
        $this->db->select();
        $this->db->from('historico_acesso');
        
        $this->db->where('id_usuario', $id_usuario );
        
        $this->db->order_by('data', " DESC");
        $this->db->order_by('id_situacao_acesso', 'DESC');
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    public function selec_historico_usuario_by_situacao($id_usuario, $id_situacao)
    {
        $this->db->select();
        $this->db->from('historico_acesso');
        
        $this->db->where('id_usuario', $id_usuario );
        $this->db->where('id_situacao_acesso', $id_situacao );
        $this->db->where('data BETWEEN "'. date('Y-m-d'). '" AND "'. date('Y-m-d 23:59:59').'"' );
        
        $query = $this->db->get();
        
        return $query->result_array();
    }

}