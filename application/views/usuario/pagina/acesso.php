<style>
/***
Bootstrap4 Card with Tabs by @mdeuerlein
***/

body {
    background-color: #f7f8f9;
}

.card {
    background-color: #ffffff;
    border: 1px solid rgba(0, 34, 51, 0.1);
    box-shadow: 2px 4px 10px 0 rgba(0, 34, 51, 0.05), 2px 4px 10px 0 rgba(0, 34, 51, 0.05);
    border-radius: 0.15rem;
}

/* Tabs Card */

.tab-card {
  border:1px solid #eee;
}

.tab-card-header {
  background:none;
}
/* Default mode */
.tab-card-header > .nav-tabs {
  border: none;
  margin: 0px;
}
.tab-card-header > .nav-tabs > li {
  margin-right: 2px;
}
.tab-card-header > .nav-tabs > li > a {
  border: 0;
  border-bottom:2px solid transparent;
  margin-right: 0;
  color: #737373;
  padding: 2px 15px;
}

.tab-card-header > .nav-tabs > li > a.show {
    border-bottom:2px solid #007bff;
    color: #007bff;
}
.tab-card-header > .nav-tabs > li > a:hover {
    color: #007bff;
}

.tab-card-header > .tab-content {
  padding-bottom: 0;
}

@media only screen and (min-width: 768px) {
 .horarios{
        width: 25% !important
    }
}
    
@media only screen and (max-width: 768px) {
    .horarios{
        width: auto !important
    }
}
</style>
<div style="margin-top: 4rem;"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card mt-3 tab-card">
                <div class="card-header tab-card-header">
                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Entrada</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Almoço</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">Saída</a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                        <!--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>-->
                        <?php 
                            if( isset($historico[0]['id_situacao_acesso']) && ($historico[0]['id_situacao_acesso']==1))
                            {
                                echo '<div class="alert alert-warning">
                                        <h5>Horário de Entrada informado:<strong> '.date('H:i', strtotime($historico[0]['hora_inicial']) ).'h</strong></h5>
                                    </div>';
                            }
                            else
                            {
                                echo '<h2 class="card-title">Informe o Horário de Entrada</h2>
                                        <form method="POST" action="register">
                                            <div class="horarios form-group">
                                                <label for="timepicker-almoco-saida">Hora da sua Entrada:</label>
                                                <input type="hidden" name="situacao" value="1">
                                                <input required placeholder="Clique no Ícone do relógio" id="timepicker-entrada" name="h_entrada" />
                                            </div>
                                            <button type="submit" class="btn btn-primary">Enviar</button>              
                                        </form>';
                            }
                        ?>    
                        
                    </div>
                    <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                        <?php
                            if( isset($historico[1]['id_situacao_acesso']) && ($historico[1]['id_situacao_acesso']==2))
                            {
                                echo '<div class="alert alert-warning">
                                        <h5>Horário informado para saída do Almoço:<strong> '.date('H:i', strtotime($historico[1]['hora_inicial']) ).'h</strong></h5>
                                            <h5>Horário informado para o retorno do Almoço:<strong> '.date('H:i', strtotime($historico[1]['hora_final']) ).'h</strong></h5>
                                    </div>';
                            }
                            else
                            {
                                echo '<h2 class="card-title">Informe o Horário de Intervalo do seu Almoço</h2>
                                    <form method="POST" action="register">
                                        <input type="hidden" name="situacao" value="2">
                                    <div class="horarios form-group">
                                        <label for="timepicker-almoco-saida">Saída:</label>
                                        <input required placeholder="Clique no Ícone do relógio" id="timepicker-almoco-saida" name="h_almoco_saida" />
                                    </div>
                                    <div class="horarios form-group">
                                        <label for="timepicker-almoco-retorno">Retorno:</label>
                                        <input required placeholder="Clique no Ícone do relógio" id="timepicker-almoco-retorno" name="h_almoco_retorno" />
                                    </div>
                                    <button type="submit" class="btn btn-primary">Enviar</button>              
                                    </form>';
                            }
                        ?>
                    </div>
                    <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                        <?php
                            if( isset($historico[2]['id_situacao_acesso']) && ($historico[2]['id_situacao_acesso']==3))
                            {
                                echo '<div class="alert alert-warning">
                                        <h5>Horário informado para saída do Almoço:<strong> '.date('H:i', strtotime($historico[1]['hora_inicial']) ).'h</strong></h5>
                                            <h5>Horário informado para o retorno do Almoço:<strong> '.date('H:i', strtotime($historico[1]['hora_final']) ).'h</strong></h5>
                                    </div>';
                            }
                            else
                            {
                                echo '<h2 class="card-title">Informe o Horário de Saída da Empresa</h2>
                                        <form method="POST" action="register">
                                            <input type="hidden" name="situacao" value="3">
                                            <div class="horarios form-group">
                                                <label for="timepicker-saida">Saída da Empresa:</label>
                                                <input required placeholder="Clique no Ícone do relógio" id="timepicker-saida" name="h_saida" />
                                            </div>
                                            <button type="submit" class="btn btn-primary">Enviar</button>              
                                        </form>';
                            }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
        $('#timepicker-entrada').timepicker({
            uiLibrary: 'bootstrap4'
        });
        
        $('#timepicker-almoco-saida').timepicker({
            uiLibrary: 'bootstrap4'
        });
        
        $('#timepicker-almoco-retorno').timepicker({
            uiLibrary: 'bootstrap4'
        });
        
        $('#timepicker-saida').timepicker({
            uiLibrary: 'bootstrap4'
        });
</script>