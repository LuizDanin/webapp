<style>
    .agenda {  }

/* Dates */
.agenda .agenda-date { width: 170px; }
.agenda .agenda-date .dayofmonth {
  width: 40px;
  font-size: 36px;
  line-height: 36px;
  float: left;
  text-align: right;
  margin-right: 10px; 
}
.agenda .agenda-date .shortdate {
  font-size: 0.75em; 
}


/* Times */
.agenda .agenda-time { width: 140px; } 


/* Events */
.agenda .agenda-events {  } 
.agenda .agenda-events .agenda-event {  } 

@media (max-width: 767px) {
    
}
</style>
<div style="margin-top: 4rem;"></div>
<div class="container">

<h2>Agenda</h2>
    <p class="lead">
        Aqui você pode ver seus historicos de acessos informados no seu dia-a-dia
    </p>
    
    <hr />
    <div class="agenda">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Hora</th>
                        <th>Evento</th>
                    </tr>
                </thead>
                <tbody>
                    <?php echo $agenda;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
