-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 03-Out-2020 às 00:15
-- Versão do servidor: 8.0.21
-- versão do PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `webapp_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_usuario`
--

CREATE TABLE `categoria_usuario` (
  `id` int NOT NULL,
  `nome` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Extraindo dados da tabela `categoria_usuario`
--

INSERT INTO `categoria_usuario` (`id`, `nome`) VALUES
(1, 'estagiario'),
(2, 'gerente');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico_acesso`
--

CREATE TABLE `historico_acesso` (
  `id` int NOT NULL,
  `id_usuario` int NOT NULL,
  `id_situacao_acesso` int NOT NULL,
  `hora_inicial` time DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `is_robot` tinyint(1) NOT NULL,
  `is_mobile` tinyint(1) NOT NULL,
  `agent_string` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `plataforma` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `navegador` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `sistema` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Extraindo dados da tabela `historico_acesso`
--

INSERT INTO `historico_acesso` (`id`, `id_usuario`, `id_situacao_acesso`, `hora_inicial`, `hora_final`, `ip`, `is_robot`, `is_mobile`, `agent_string`, `plataforma`, `navegador`, `sistema`, `data`) VALUES
(1, 1, 1, '08:30:00', NULL, '177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-01 21:40:08'),
(2, 1, 2, '12:05:00', '14:05:00', '177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-01 22:02:46'),
(3, 1, 3, NULL, '18:05:00', '177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-01 22:03:13'),
(4, 1, 1, '08:30:00', NULL, '177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-02 19:47:55'),
(5, 1, 2, '12:05:00', '14:05:00', '177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-02 20:42:02'),
(6, 1, 3, NULL, '20:20:00', '177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-02 20:55:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacao_acesso`
--

CREATE TABLE `situacao_acesso` (
  `id` int NOT NULL,
  `nome` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Extraindo dados da tabela `situacao_acesso`
--

INSERT INTO `situacao_acesso` (`id`, `nome`) VALUES
(1, 'inicio'),
(2, 'almoco'),
(3, 'saida');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int NOT NULL,
  `id_categoria_usuario` int NOT NULL,
  `nome` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `senha` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `id_categoria_usuario`, `nome`, `email`, `senha`) VALUES
(1, 1, 'Luiz Danin', 'otaviodanin@gmail.com', 'luizdanin'),
(2, 2, 'Gerente', 'gerente@mail.com', 'gerente');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `categoria_usuario`
--
ALTER TABLE `categoria_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `historico_acesso`
--
ALTER TABLE `historico_acesso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_historico_acesso_estagiario1_idx` (`id_usuario`),
  ADD KEY `fk_historico_acesso_situacao_acesso1_idx` (`id_situacao_acesso`);

--
-- Índices para tabela `situacao_acesso`
--
ALTER TABLE `situacao_acesso`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_categoria_usuario1_idx` (`id_categoria_usuario`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `categoria_usuario`
--
ALTER TABLE `categoria_usuario`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `historico_acesso`
--
ALTER TABLE `historico_acesso`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `situacao_acesso`
--
ALTER TABLE `situacao_acesso`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `historico_acesso`
--
ALTER TABLE `historico_acesso`
  ADD CONSTRAINT `fk_historico_acesso_estagiario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `fk_historico_acesso_situacao_acesso1` FOREIGN KEY (`id_situacao_acesso`) REFERENCES `situacao_acesso` (`id`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_categoria_usuario`) REFERENCES `categoria_usuario` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
